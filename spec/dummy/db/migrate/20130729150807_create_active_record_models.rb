class CreateActiveRecordModels < ActiveRecord::Migration
  def change
    create_table :active_record_models do |t|
      t.string :name
      t.string :data_type
      t.integer :order
      t.integer :width

      t.timestamps
    end
  end
end
