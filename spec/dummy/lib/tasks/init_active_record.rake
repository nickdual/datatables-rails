namespace :db do
  namespace :active_record do
    task :init, [ ] => :environment do |t, args|
      (0..10).each do |i|
        obj = ActiveRecordModel.new({:name => "name#{i}", :data_type => 'int', :order => i, :width => (i%3 * 20)})
        obj.save
      end
      (10..20).each do |i|
        obj = ActiveRecordModel.new({:name => "name#{i}", :data_type => 'string', :order => i, :width => (i%3 * 20)})
        obj.save
      end
      (20..25).each do |i|
        obj = ActiveRecordModel.new({:name => "name", :data_type => 'int', :order => i, :width => (i%3 * 20)})
        obj.save
      end
      (25..30).each do |i|
        obj = ActiveRecordModel.new({:name => "name", :data_type => 'string', :order => i, :width => (i%3 * 20)})
        obj.save
      end
    end
  end
end