class WelcomeController < ApplicationController
  include DatatablesRails

  def index
    
  end
  
  def static
    
  end

  def show
    @obj = MongoModel.find(params[:id])
    render @obj
  end

  def active_record_update
    @obj = ActiveRecordModel.find(params[:id])
    @obj.name = params[:name]
    @obj.data_type = params[:data_type]
    @obj.width = params[:width]
    @obj.order = params[:order]
    @obj.save()
    data =  [   "<input type=\"checkbox\" id=\"#{@obj.id}\" class=\"chk_select\">",
          @obj.name,
          @obj.data_type,
          @obj.width,
          @obj.order,
          "<span>#{@obj.order.to_i + @obj.width.to_i}</span>"   ]

    render :json => data
  end

  def active_record_delete
    @obj = ActiveRecordModel.find(params[:id])
    @obj.destroy
    render :json => :nothing
  end

  def mongo_update
    @obj = MongoModel.find(params[:id])
    @obj.name = params[:name]
    @obj.data_type = params[:data_type]
    @obj.width = params[:width]
    @obj.order = params[:order]
    @obj.save()
    puts @obj.to_json
    data = @obj.to_a.map do |obj|
      [   "<input type=\"checkbox\" id=\"#{obj.id}\" class=\"chk_select\">",
          obj.name,
          obj.data_type,
          obj.width,
          obj.order,
          "<span>#{obj.order.to_i + obj.width.to_i}</span>",

      ]
    end
    render :json => data[0]
  end

  def mongo_delete
    @obj = MongoModel.find(params[:id])
    @obj.destroy
    render :json => :nothing
  end

  def mongo
    sSearch = params[:sSearch]
    if sSearch.present?
      sSearch = sSearch.downcase.squeeze(' ').strip
      text_search = Regexp.compile(Regexp.escape(sSearch), Regexp::IGNORECASE)
      @objs = MongoModel.any_of({name: text_search}, {data_type: text_search} )
    else
      @objs = MongoModel.all
    end
    @data_type =  @objs.pluck("data_type").uniq
    @data_order =  @objs.pluck("order").uniq
    @datatable_cfg = MongoidDatatable.new(view_context, @objs, {
      :add_checkbox => true,
      :columns => ["name", "data_type", "order","width"],
      :type => [{'type' => 'string'},{'type' => 'string'},{'type' => 'integer'},{'type' => 'integer'}]
      })
    add_dynamic_columns(@datatable_cfg)
    @column_configs = @datatable_cfg.get_columns_config
    respond_to do |format|
      format.html
      format.json { render json: @datatable_cfg }
    end
  end
  def active_record
    sSearch = params[:sSearch]
    if sSearch.present?
      sSearch = sSearch.downcase.squeeze(' ').strip
      text_search = "%#{sSearch}%"
      @objs = ActiveRecordModel.where("name like ? OR data_type like ? ", text_search, text_search )
    else
      @objs = ActiveRecordModel.where("id IS NOT NULL")
    end
    table_name =  ActiveRecordModel.table_name
    @data_type = ActiveRecordModel.pluck(:data_type).uniq
    @data_order =  ActiveRecordModel.pluck(:order).uniq
    @datatable_cfg = ActiveRecordDatatable.new(view_context, @objs, {
      :add_checkbox => true,
      :columns => ["name", "data_type", "order", "width"],
      :type => [{'type' => 'string'},{'type' => 'string'},{'type' => 'integer'}, {'type' => 'integer'}],
      :table_name => table_name
      })
    add_dynamic_columns(@datatable_cfg)
    @column_configs = @datatable_cfg.get_columns_config
    respond_to do |format|
      format.html
      format.json { render json: @datatable_cfg }
    end
  end
  def get_selected_rows
    data = params[:dataRow]
    render :json => data
  end

  private
  def add_dynamic_columns(datatable_cfg)
    datatable_cfg.dynamic_columns do
      add_at "total", 5 do
        config do |dt|
          cfg = dt.get_column_config("total")
          cfg[:width] = 10
          cfg[:name] = "total"

          cfg
        end
        data do |dt, obj, idx|
          "<span>#{obj.order.to_i + obj.width.to_i}</span>"
        end
      end
    end

    datatable_cfg.dynamic_columns do
      add_at "action", 6 do
        config do |dt|
          cfg = dt.get_column_config("action")
          cfg[:width] = "50px"
          cfg
        end
        data do |dt, obj, idx|
          "<a href=\"\" id=\"#{obj.id}\" class=\"view\">#{I18n.t("action_view")}</a> /<a href=\"\" id=\"#{obj.id}\" class=\"editor_edit\">#{I18n.t("action_edit")}</a> / <a href=\"\" id=\"#{obj.id}\" class=\"editor_remove\">#{I18n.t("action_delete")}</a>"
        end
      end
    end
  end
end