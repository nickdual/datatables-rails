class ActiveRecordModel < ActiveRecord::Base
  attr_accessible :data_type, :name, :order, :width
end
