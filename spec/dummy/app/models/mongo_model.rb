class MongoModel
  include Mongoid::Document

  field :name, :type => String
  field :data_type, :type => String, :default => 'string'
  field :order, :type => Integer, :default => 0
  field :width, :type => Integer, :default => '20'

end