# encoding: UTF-8
module DatatablesRails
  
  #this calss hold dynamic column configuration
  class DynColumnDefDatatable
    attr_accessor :config_block
    attr_accessor :data_block
    attr_accessor :col_name
    attr_accessor :insert_pos
    
    def initialize(name, position)
      @col_name = name
      @insert_pos = Hash.new
      if position.is_a? Integer     
        @insert_pos[:type] = :at
        @insert_pos[:pos] = position
      elsif position.is_a? Hash
        @insert_pos[:type] = position[:type]
        @insert_pos[:pos] = position[:pos]
      end
      
      raise "invalid dyncol: #{name}, params #{position.inspect}" if @insert_pos.size != 2 or @insert_pos[:type].blank? or @insert_pos[:pos].blank?
      raise "invalid dyncol: #{name}, position #{position.inspect}" if @insert_pos[:type] != :at and @insert_pos[:type] != :before       
    end
    
    def config(&block)
      self.config_block = block 
    end
    
    def data(&block)
      self.data_block = block 
    end
    
    def insert_after?(index)
      insert = false
      if insert_pos[:type] == :at
        insert = (index <= insert_pos[:pos])
      end
      insert
    end
    
    def insert_here?(col_name, index)
      insert = false
      if insert_pos[:type] == :at
        insert = (index == insert_pos[:pos])  
      elsif insert_pos[:type] == :before
        insert = (@insert_pos[:pos] == col_name)
      end
      insert
    end
    
    def get_config(datatable)
      raise ArgumentError, "config_block must be given for dynamic column:#{col_name} inserted at position #{insert_pos.inspect}" if self.config_block.nil?
      
      instance_exec datatable, &self.config_block
    end
    
    def get_data(datatable, object, index)   
      raise ArgumentError, "data_block must be given for dynamic column:#{col_name} inserted at position #{insert_pos.inspect}" if self.data_block.nil?
  
      instance_exec datatable, object, index, &self.data_block
    end
  end
end