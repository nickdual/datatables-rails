# encoding: UTF-8
module DatatablesRails
  class MongoidDatatable < BaseDatatable
    delegate :params, :h, :link_to, :number_to_currency, to: :@view
  
    def initialize(view, scope, options = {})
      @view = view
      @scope = scope
      @columns = {}
      unless options[:columns].blank?
        if options[:columns].is_a? Array
          options[:columns].each_with_index do |col, index|
            @columns.merge!({col => options[:type][index]})
          end
        else
          @columns.merge!(options[:columns])    
        end
      end
      @add_checkbox = true if options[:add_checkbox] == true
      super
    end
  
    def as_json(options = {})
      size = objects.size
      {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: @scope.size,
        iTotalDisplayRecords: size,
        aaData: data(@add_checkbox)
      }
    end
      
    def get_column_config(col_name)
      col_config = super
      unless (@columns[col_name].blank? )
        col_config.merge!(@columns[col_name])
      end
      col_config

    end

  private

    def get_raw_columns
      @columns.keys
    end
    
    def get_object_values(object, idx)
      values_array = super
    end
    
    def add_meta_columns(object, ret_hash, idx)
      ret_hash
    end
    
    def fetch_objects
      objects = @scope.order_by("#{params_sort_by} ")
      objects = objects.page(page).per(per_page)
      get_columns.each_with_index do |col_name, index|
        value = ""
        if params["sSearch_#{index}".to_sym].blank? == false
          value = params["sSearch_#{index}".to_sym]
        end
        unless value.blank?
          col_config = @columns[col_name]
          unless col_config.blank?
            if col_config["type"] == "integer"
              objects = apply_integer_criteria(objects, col_name, value)
            else
              #like statement
              value = Regexp.compile(Regexp.escape(value), Regexp::IGNORECASE)
              objects = objects.where({col_name => value})
            end
          else
            #like statement
            value = Regexp.compile(Regexp.escape(value), Regexp::IGNORECASE)
            objects = objects.where({col_name => value})
          end
        end
      end
      objects
    end
    
  end
end