#UTF8

module DatatablesRails
  module Helpers
    module FormHelper
      def datatable(columns, opts={})
        path = File.expand_path("../../../locales/#{I18n.locale}.txt", __FILE__)
        data = File.open(path)
        i18_n = data.read
        sort_by = opts[:sort_by] || nil
        additional_data = opts[:additional_data] || {}
        search_data_string = opts[:search_data] || {}
        search_label = opts[:search_label] || "Recherche"
        processing = opts[:processing] || "Processing"
        table_dom_id = opts[:table_dom_id] ? "##{opts[:table_dom_id]}" : ".datatable"
        no_records_message = opts[:no_records_message] || nil
        row_callback = opts[:row_callback] || nil
        dom_config = opts[:dom_config].blank? ? "" : "sDom: \"#{opts[:dom_config]}\","
        config_append = opts[:config_append] || nil
        append = opts[:append] || nil
        ajax_source = opts[:ajax_source] || nil
        submit_row_url = opts[:submit_row_url] || nil
        button_submit_text = opts[:button_submit_text] || nil
        additional_data_string = ""
        additional_data.each_pair do |name, value|
          additional_data_string = additional_data_string + ", " if !additional_data_string.blank? && value
          additional_data_string = additional_data_string + "{'name': '#{name}', 'value':'#{value}'}" if value
        end
        tableTools = ""
        row_selected = "'',"
        unless opts[:table_tools].blank?
          tableTools = %Q{
            "oTableTools": {
              "aButtons": [],
              "sRowSelect": "multi",
              #{handle_simple_param("fnRowSelected", opts[:table_tools][:row_selected])}
              #{handle_simple_param("fnRowDeselected", opts[:table_tools][:row_deselected])}
              #{handle_simple_param("fnPreRowSelect", opts[:table_tools][:row_preselect])}
            },
          }
          #row_selected = opts[:table_tools][:row_selected] unless opts[:table_tools][:row_selected].blank?
          #row_deselected = opts[:table_tools][:row_deselected] unless opts[:table_tools][:row_deselected].blank?  
        end
        %Q{
             columns = [#{formatted_columns(columns)}];
             oTable = $('#{table_dom_id}').dataTable({
              #{dom_config}
              #{ handle_simple_param("sPaginationType", opts[:pagination_type] || "bootstrap") }
              #{ handle_simple_param("iDisplayLength", opts[:per_page] || opts[:display_length]|| 25) }
              #{ handle_simple_param("bProcessing", true) }
              #{ handle_simple_param("bServerSide", opts[:ajax_source].present?) }
              #{ handle_simple_param("bLengthChange", opts[:length_change] || false) }
              #{ handle_simple_param("bJQueryUI", opts[:jquery_ui] || false) }
              #{ handle_simple_param("bStateSave", opts[:persist_state].present? ? opts[:persist_state].to_s : "true" ) }
              #{ handle_simple_param("bFilter", opts[:search].present? ? opts[:search].to_s : "true" ) }
              #{ handle_simple_param("fnDrawCallback", opts[:draw_callback]) }
              #{ handle_simple_param("bAutoWidth", opts[:auto_width], "true") }
              #{"'aaSorting': #{sort_by}," if sort_by}
              #{"'sAjaxSource': '#{ajax_source}'," if ajax_source}
              "oColVis": {},
              "aoColumns": [#{formatted_columns(columns)}],
              #{"'fnRowCallback': function( nRow, aData, iDisplayIndex ) { #{row_callback} }," if row_callback}
              #{handle_simple_param("sServerMethod", opts[:server_method])}
              #{"'fnServerData': function ( sSource, aoData, fnCallback, oSettings ) {
                aoData.push( #{additional_data_string} );
                aoData.push( #{search_data_string} );
                console.log(aoData)
                method = oSettings.sServerMethod;
                $.ajax({
                  type: method,
                  url: eval(sSource),
                  data: aoData,
                  success: function (json) {fnCallback(json);},
                  dataType: 'JSON',
                  cache: false
                });
              }," if opts[:ajax_source].present? }
              #{tableTools}
              #{config_append}
              oLanguage: #{i18_n},
              "oTableTools": {
                "aButtons": [
                {
                    "sExtends": "copy",
                     #{handle_simple_param("sButtonText", opts[:buttonCopy] || 'Copy' , nil, "string")}
                },
                {
                    "sExtends": "csv",
                    #{handle_simple_param("sButtonText", opts[:buttonCSV] || 'CSV' , nil, "string")}
                },{
                    "sExtends": "xls",
                      #{handle_simple_param("sButtonText", opts[:buttonXSL] || "XLS" , nil, "string")}
                },{
                    "sExtends": "pdf",
                     #{handle_simple_param("sButtonText", opts[:buttonPDF] || 'PDF' , nil, "string")}
                },{
                    "sExtends": "print",
                      #{handle_simple_param("sButtonText", opts[:buttonPrint] || 'Print' , nil, "string")}
                }

                ],
                #{handle_simple_param("sRowSelect", opts[:sRowSelect] || nil)}
              },
              "oColVis":
                  { #{handle_simple_param("buttonText", opts[:buttonText] || nil , nil, "string")} }
            })#{append};
        }.html_safe

      end

      def filter_config(columns, opts={})
        i = 0
        #sRangeFormat: "De {from} à {to}",

        columns_str = columns.map { |c|
          i += 1
          if c.nil? or c.empty? or c[:type].blank? or c[:searchable].to_s.downcase == "false"
            "null"
          else
            unless c[:filter_type].blank?
              type = c[:filter_type]
            else
              type = c[:type]
              if (%w( integer float ).include? type)
                type = "number-range"
              elsif (type == "string")
                type = "text"
              end
            end
            %Q{
                {
                  #{handle_simple_param("values", c[:values])} 
                  type: '#{type}'
                }
              }
          end
        }.join(",")

        %Q{.columnFilter({
              #{handle_simple_param("sPlaceHolder", "head:before")}
              #{handle_simple_param("bUseColVis", true)}
              #{handle_simple_param("sRangeFormat", opts[:filter_range_format]) if opts[:filter_range_format]}
              aoColumns: [
                #{columns_str}
              ]
            });
          }
      end

      private
      def custom_filter(columns, opts={})
        i = 0
        #sRangeFormat: "De {from} à {to}",
        columns_str = columns.map { |c|

          i += 1
          if c.nil? or c.empty? or c[:type].blank? or c[:searchable].to_s.downcase == "false"
            "null"
          else
            unless c[:filter_type].blank?
              type = c[:filter_type]
            else
              type = c[:type]
              if (%w( integer float ).include? type)
                type = "number-range"
              elsif (type == "string")
                type = "text"
              end
            end
            %Q{
                {
                  #{handle_simple_param("values", c[:values])}
                  type: '#{type}'
                }
              }
          end
        }.join(",")
        return columns_str
      end
      def handle_simple_param(dt_name, param_value, default_value = nil, force_type = nil)
        #make sure false is process correctly by converting it to string
        param_value = param_value.to_s
        if param_value.blank? and default_value.nil? == false
          "'#{dt_name}' : #{default_value},"
        elsif param_value.blank? == false
          if (dt_name[0] == "s" || force_type == "string")
            "'#{dt_name}' : '#{param_value}',"
          else
            "'#{dt_name}' : #{param_value},"
          end
        else
          ""
        end
      end

      def formatted_columns(columns)
        i = 0
        columns.map { |c|
          i += 1
          if c.nil? or c.empty?
            "null"
          else
            "{
            #{handle_simple_param("sTitle", c[:title])}
            #{handle_simple_param("sName", c[:name])}
            #{handle_simple_param("bVisible", c[:visible], true)}
            #{handle_simple_param("sType", c[:type] || "string" )}
            #{handle_simple_param("sWidth", c[:width].to_s.present? ? c[:width].to_s : "50px" )}
            #{handle_simple_param("bSortable", c[:sortable].to_s.present? ? c[:sortable].to_s : "true" )}
            #{handle_simple_param("bSearchable", c[:searchable].to_s.present? ? c[:searchable].to_s : "true" )}
            #{handle_simple_param("sClass", c[:class]) if c[:class]}
            #{handle_simple_param("sDefaultContent", c[:defaultContent]) if c[:defaultContent]}
            }"
          end
        }.join(",")
      end
    end
  end
end
