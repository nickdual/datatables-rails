# encoding: UTF-8
module DatatablesRails
  class ActiveRecordDatatable < BaseDatatable
    delegate :params, :h, :link_to, :number_to_currency, to: :@view
  
    def initialize(view, scope, options = {})
      @view = view
      @scope = scope
      @columns = {}
      @table_name = options[:table_name]
      unless options[:columns].blank?
        if options[:columns].is_a? Array
          options[:columns].each_with_index do |col, index|
            @columns.merge!({col => options[:type][index]})
          end
        else
          @columns.merge!(options[:columns])    
        end
      end
      @add_checkbox = true if options[:add_checkbox] == true
      super
    end
  
    def as_json(options = {})
      size = objects.size
      {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: @scope.size,
        iTotalDisplayRecords: size,
        aaData: data(@add_checkbox)
      }
    end
      
    def get_column_config(col_name)
      col_config = super
      unless (@columns[col_name].blank? )
        col_config.merge!(@columns[col_name])
      end
      col_config
      
    end
    
  private
    def apply_integer_criteria_active_record(query, col_name, criteria_str)
      if criteria_str.include?("~")
        range = criteria_str.strip.split("~")

        value_1, value_2 = nil
        value_1 = range[0].to_i if range.size >= 1 and valid_integer?(range[0])
        value_2 = range[1].to_i if range.size >= 2 and valid_integer?(range[1])

        if (value_1.nil? == false) and (value_2.nil? == false)
          query = query.where("#{col_name} BETWEEN  #{value_1} AND #{value_2}")
        elsif (value_1.nil? == false)
          query = query.where("#{col_name} >= #{value_1}")
        elsif (value_2.nil? == false)
          query = query.where("#{col_name} <= #{value_2}")
        end
      else

        query = query.where({ col_name => criteria_str})
      end
      query
    end
    def get_raw_columns
      @columns.keys
    end
    
    def get_object_values(object, idx)
      values_array = super
    end
    
    def add_meta_columns(object, ret_hash, idx)
      ret_hash
    end
    
    def fetch_objects
      objects = @scope.order("#{params_sort_by(@table_name)}")
      objects = objects.page(page).per(per_page)
      get_columns.each_with_index do |col_name, index|
        value = ""
        if params["sSearch_#{index}".to_sym].blank? == false
          value = params["sSearch_#{index}".to_sym]
        end
        unless value.blank?
          col_config = @columns[col_name]
          unless col_config.blank?
            if col_config["type"] == "integer"
              objects = apply_integer_criteria_active_record(objects, col_name, value)
            else
              #like statement
              text_search = "%#{value}%"
              objects = objects.where("#{col_name} like ?" , text_search)
            end
          else
            #like statement
            text_search = "%#{value}%"
            objects = objects.where("#{col_name} like ?" , text_search)
          end
        end
      end
      objects
    end
  end
end