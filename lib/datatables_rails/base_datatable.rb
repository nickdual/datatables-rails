# encoding: UTF-8
module DatatablesRails
  
  class BaseDatatable
    attr_accessor :add_checkbox
    attr_accessor :dyn_columns
    def initialize(*args)
      @add_checkbox ||= false
      @dyn_columns = Array.new
      if @add_checkbox
        dynamic_columns do
          add_at "chk_box", 0 do
            config do |dt, obj|
              dt.get_checkbox_config
            end
            data do |dt, obj, idx|
              dt.get_checkbox_value(obj)
            end
          end
        end
      end
    end
    
    def as_hash(addition_columns = true)
      data(addition_columns)
    end
    
    def data(addition_columns = true)
      retlist = []
      objects.each_with_index do |cur_obj, obj_idx|
        ret_hash = {}
        tmp_­array = get_object_values(cur_obj, obj_idx)
        
        tmp_­array.each_with_index do |item, idx|
          ret_hash[idx] = item
        end
        
        if addition_columns
          retlist << add_meta_columns(cur_obj, ret_hash, obj_idx)  
        else
          retlist << ret_hash
        end
         
      end

      retlist
    end
    
    
    def objects
      @objects ||= fetch_objects
    end
    def apply_integer_criteria(query, col_name, criteria_str)
      if criteria_str.include?("~")
        range = criteria_str.strip.split("~")

        value_1, value_2 = nil
        value_1 = range[0].to_i if range.size >= 1 and valid_integer?(range[0])
        value_2 = range[1].to_i if range.size >= 2 and valid_integer?(range[1])

        if (value_1.nil? == false) and (value_2.nil? == false)
          query = query.where({ col_name => {'$gte' => value_1, '$lte' => value_2}})
        elsif (value_1.nil? == false)
          query = query.where({ col_name => {'$gte' => value_1}})
        elsif (value_2.nil? == false)
          query = query.where({ col_name => {'$lte' => value_2}})
        end
      else

        query = query.where({ col_name => criteria_str})
      end
      query
    end

    
    def page
      params[:iDisplayStart].to_i/per_page + 1
    end
  
    def per_page
      params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
    end
  
    def sort_column
      get_columns[params[:iSortCol_0].to_i]
    end
  
    def sort_direction
      params[:sSortDir_0] == "desc" ? "desc" : "asc"
    end
    def params_sort_by(table_name = nil)
      temp = []
      sort_by = []
      (0..params[:iColumns].to_i - 1).each do |i|
        temp.clear();
        if params[:"iSortCol_#{i}"]
          if get_columns[params[:iSortCol_0].to_i] != 'chk_box'
            column =   get_columns[params[:"iSortCol_#{i}"].to_i]
          else
            column = get_columns[params[:"iSortCol_#{i}"].to_i + 1]
          end
          column = temp.push(table_name , column).join('.') if table_name.present?
          temp.clear()
          sort_dir = params[:"sSortDir_#{i}"] == "desc" ? "desc" : "asc"
          temp.push(column,sort_dir)
          sort_by.push(temp.join(' '))
        end
      end
      sort_by = sort_by.join(' , ')

      return sort_by
    end
    def get_columns_config
      columns_configs = []
      get_columns.each do |name|
        if is_dyn_column?(name)
          col = get_dyn_column(name)
          columns_configs << col.get_config(self)
        else
          col_config = get_column_config(name)
          columns_configs << col_config
        end
      end
      columns_configs
    end
    
    def get_column_config(col_name)
      
      col_config = {
        :width => "50px",
        :name => col_name,
        :class => col_name,
        :title => col_name.humanize
      }
      
      if is_dyn_column?(col_name)
        col_config.merge!({
          :sortable => false,
          :searchable => false
        })
      end
      
      col_config
    end
    
    #this function return the texts to be used to create the table header row
    def get_column_texts(col_name)
      configs = get_columns_config
      config_idx = configs.index{|c| c[:name] == col_name}
      raise ArgumentError, "cannot get text for column '#{col_name}' without a config. config_list: #{configs}" if config_idx.nil?
      
      config = configs[config_idx]
      ret_text = {}
      title = (config[:title] || config[:name].capitalize)
      ret_text[:title] = title
      if title.size > 8
        ret_text[:short_title] = title[0..4]
      else
        ret_text[:short_title] = title
      end
      
      ret_text[:category] = ""
      
      ret_text
    end
    
    def get_columns
      if (@cols.nil?)
        
        @cols = []
        last_index = 0
        
        get_raw_columns.each_with_index do |col_name, index|
          inserts = @dyn_columns.select{|d| d.insert_here?(col_name, index) }
          inserts.each do |dyn_col_to_insert_here| 
            @cols << dyn_col_to_insert_here.col_name        
          end 
          last_index = index
          @cols << col_name
        end
        
        #handle dyn columns to insert after 
        inserts = @dyn_columns.select{|d| d.insert_after?(last_index) }
        inserts.each do |dyn_col_to_insert_here| 
            @cols << dyn_col_to_insert_here.col_name        
          end
      end 
      @cols
    end
    
    def get_column_index(name)
      get_columns.index do |cur_col|
        cur_col == name
      end
    end
    
    def get_object_values(object, idx)
      values_array = []
      get_columns.each do |name| 
        if is_dyn_column?(name)
          col = get_dyn_column(name)
          values_array << col.get_data(self, object, idx)
        else
          if (object.respond_to?(name.to_sym))
            val = object.send(name)
            val = format_value(val, name, object, idx)
            values_array << val
          else
            #this should not happen
            values_array << "#{name}"
          end
        end
      end
      values_array
    end
    
    def format_value(value, col_name, object, index)
      if (value.is_a?(Float))
        value = value.to_s[0..3]
      else
        value = value.to_s.force_encoding("UTF-8")
      end
      return value
    end
    
    def get_header_columns(only_visible = false)
      ret_cols = []
      get_columns.each do |field|
        if only_visible == false
          ret_cols << field
        elsif get_column_to_remove.index{|c| c[:name] == field}.nil?
          ret_cols << field
        end
      end
      
      ret_cols
    end
    
    def is_dyn_column?(col_name)
      (@dyn_columns.index{|c| c.col_name == col_name}.nil? == false)
    end
    
    def get_dyn_column(col_name)
      idx = @dyn_columns.index{|c| c.col_name == col_name}
      raise "cannot find dyn column #{col_name} in configured dyn columns: #{@dyn_columns.inspect}" if idx.nil?
      @dyn_columns[idx]
    end
    
    def remove_hidden_columns(row_hash)
      ret_hash = row_hash.dup
      get_column_to_remove.each do |c|
        ret_hash.delete(c[:index])
      end
      ret_hash
    end
    
    def get_raw_columns
      raise NotImplementedError.new("You must implement get_raw_columns.")
    end
    
    def get_column_to_remove
      if @col_to_remove.nil?
        #grab visible columns params
        vis_col = params["visColumns"].split(',').reject(&:empty?)
        all_columns = get_columns
        tmp_cols = all_columns - vis_col
        @col_to_remove = []
        tmp_cols.each do |col|
          @col_to_remove << {
            :name => col,
            :index => all_columns.index(col)
          }
        end
      end
      @col_to_remove
    end
    
    def dynamic_columns(&block)
      unless block.nil?
        instance_exec &block
      end
    end
    
    def add_at(name, position, &block)
      unless block.nil?
        dyn_col = DynColumnDefDatatable.new(name, position)
        dyn_col.instance_exec &block
        @dyn_columns << dyn_col
      end
    end
    
    def add_meta_columns(object, ret_hash, idx)
      raise NotImplementedError.new("You must implement add_meta_columns.")
    end
    
    def valid_integer?(value)
      begin
        Integer(value)
        return true
      rescue Exception => e
        return false
      end
    end

    def get_checkbox_config
      col_config = {
        :sortable => false,
        :searchable => false,
        :width => '10px',
        :defaultContent => '<input type="checkbox" class="chk_select" />',
        :class => 'select check_box'
      }
    end
    
    def get_checkbox_value(object)
      "<input type=\"checkbox\" id=\"#{object.id}\" class=\"chk_select\" />"
    end
  
  end
end