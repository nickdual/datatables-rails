module DatatablesRails
  class Engine < ::Rails::Engine
    initializer 'datatable_rails.initialize' do
      ActiveSupport.on_load(:action_view) do
        include DatatablesRails::Helpers::FormHelper
      end
    end
  end
end