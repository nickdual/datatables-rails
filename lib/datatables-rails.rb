require 'datatables_rails/engine' if defined?(::Rails)

module DatatablesRails
  extend ActiveSupport::Autoload
  autoload :Helpers
  autoload :BaseDatatable
  autoload :DynColumnDefDatatable
  autoload :MongoidDatatable
  autoload :ActiveRecordDatatable
end
