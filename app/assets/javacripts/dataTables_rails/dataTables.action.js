

function saveRow ( oTable, nRow, url)
{
    var inputs = $("input", $(".modal-body"));
    var key = ""
    var obj = {}

    for(var i = 0; i < inputs.length; i++ ){
        if(inputs[i].type == "hidden"){
            key = "id"
            obj[key] = inputs[i].id
        }else{
            if(inputs[i].type == "text"){
                key = inputs[i].className
                obj[key] = inputs[i].value

            }
        }
    }
    $.ajax({
        type: "Post",
        url: url,
        data: obj ,
        success: function (json) {
          console.log(json)
          for(var i = 0; i < json.length; i++ ){
            oTable.fnUpdate(json[i], nRow, i , false );
          }
        },
        dataType: 'JSON',
        cache: false
    });

}

function showRow(nRow,id){
    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    var body = '<input type="hidden" id="' + id +'"/>'
    for(var i = 0; i < jqTds.length; i++){
        if(jqTds[i].childNodes.length > 0) {
            if(jqTds[i].childNodes[0].nodeType ==  3 || jqTds[i].childNodes[0].nodeName == "SPAN" ) {
                css = columns[i].sClass
                body += '<div class="row"><label style="width: 20%;float:  left;padding-left: 20px;">'+ oTable.find('thead >tr >th')[i].textContent + ':</label><div col="'+ i +'" class="' + css +'">' +  aData[i] + '</div></div>'

            }
        }else{
            css = columns[i].sClass
            body += '<div class="row"><label style="width: 20%;float:  left;padding-left: 20px;">'+ oTable.find('thead >tr >th')[i].textContent + ':</label><div col="'+ i +'" class="' + css +'">' +  aData[i] + '</div></div>'
        }
    }

    $("#action_row .modal-title").text('Show Entry')
    $(".modal-body").html(body);
    $("#action_row .modal-footer").hide()
    $("#action_row").modal('show');
}

function editRow ( oTable, nRow, id,url )
{

    var aData = oTable.fnGetData(nRow);
    var jqTds = $('>td', nRow);
    var body = '<input type="hidden" id="' + id +'"/>'
    for(var i = 0; i < jqTds.length; i++){
        if(jqTds[i].childNodes.length > 0) {
            if(jqTds[i].childNodes[0].nodeType ==  3  ) {
                css = columns[i].sClass
                body += '<div class="row"><label style="width: 20%;float:  left;padding-left: 20px;">'+ oTable.find('thead >tr >th')[i].textContent +':</label><input type="text" col="'+ i +'"class="' + css +'" value="' + aData[i]+'"></div>'

            }
        } else
            if( jqTds[i].childNodes.length == 0){
                css = columns[i].sClass
                body += '<div class="row"><label style="width: 20%;float:  left;padding-left: 20px;">'+ oTable.find('thead >tr >th')[i].textContent +':</label><input type="text" col="'+ i +'"class="' + css +'" value="' + aData[i]+'"></div>'
            }

    }
    $("#action_row .modal-title").text('Edit Entry')
    $(".modal-body").html(body);
    $("#action_row .modal-footer").show()
    $("#action_row .modal-footer").html('<button id="save_row" class="btn btn-primary">Update</button>')
    $("#save_row").on('click', function (e){
        saveRow( oTable, nRow,url);
        $("#action_row").modal('hide');
        e.preventDefault();
    });
    $("#action_row").modal('show');
}

function deleteRow(nRow,obj,url){
    var body = 'Are you sure you wish to delete the row?'
    $("#action_row .modal-title").text('Delete Entry')
    $(".modal-body").html(body);
    $("#action_row .modal-footer").show()
    $("#action_row .modal-footer").html('<button id="delete_row" class="btn btn-primary">Delete</button>')
    $("#delete_row").on('click', function (e){
        $.ajax({
            type: "Delete",
            url: url,
            data: obj ,
            success: function (json) {
                oTable.fnDeleteRow( nRow );
            },
            dataType: 'JSON',
            cache: false
        });
        $("#action_row").modal('hide');
        e.preventDefault();
    });
    $("#action_row").modal('show');
}

