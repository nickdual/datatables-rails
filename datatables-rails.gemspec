$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "datatables_rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "datatables-rails"
  s.version     = DatatablesRails::VERSION
  s.authors     = ["Jade Tremblay"]
  s.homepage    = ""
  s.summary     = "DatatablesRails is a jquery datatables/rails integration."
  s.description = "DatatablesRails is partly based on phronos/rails_datatables"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 3.1"
  
  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "capybara"
  s.add_development_dependency "guard-rspec"
  s.add_development_dependency "guard-spork"
  
  
  s.add_development_dependency "sqlite3"
end
